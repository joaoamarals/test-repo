const btn = document.querySelector(".btn");

btn.addEventListener("click", () => {
  const container = document.querySelector(".container");
  const h2 = document.createElement("h2");
  h2.textContent = "Opa, fui clicado!";

  container.appendChild(h2);
});
